// HomeWork_19.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>


class Animal
{
private:
    int n;
public:
    Animal(int n) { this->n = n; }
    int GetNum()
    {
        return n;
    }
    virtual void Voice()
    {
        std::cout << "No Voice\n";
    }
};

class Dog: public Animal
{
public:
    Dog(int n) : Animal(n) {}
    void Voice() override
    {
        std::cout << "Dog " << GetNum() << " voice\n";
    }
};

class Cat : public Animal
{
public:
    Cat(int n) : Animal(n) {}
    void Voice() override
    {
        std::cout << "Cat " << GetNum() << " voice\n";
    }
};

class Cow : public Animal
{
public:
    Cow(int n) : Animal(n) {}
    void Voice() override
    {
        std::cout << "Cow " << GetNum() << " voice\n";
    }
};



int main()
{
    Animal* Animals[5];

    Animals[0] = new Dog(1);
    Animals[1] = new Dog(2);
    Animals[2] = new Cat(1);
    Animals[3] = new Cat(2);
    Animals[4] = new Cow(4);

    for (Animal* a : Animals)
    {
        a->Voice();
    }
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
